﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Datos
{
    public class Conexion
    {

        private string con = @"Data Source=DESKTOP-Q3AODOA;Initial Catalog=login;Integrated Security=True";

        public string Con
        {
            get
            {
                return con;
            }

            set
            {
                con = value;
            }
        }
        public SqlConnection conec;

        public SqlConnection Conecte()
        {
            try
            {
                conec = new SqlConnection(Con);
                conec.Open();
                return conec;
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al Conectar a BD" +ex.ToString());
                
            }
            conec.Close();
            return conec;

        }


    }
}

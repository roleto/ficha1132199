﻿using Logica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        Login oLogin = new Login();
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            string retorno = oLogin.ConsultarUsuarioBD(txtNickNAme.Text, txtContraseña.Text);


            switch (retorno)
            {

                case "Administrador":
                    FrmAdministrador oAdmin = new FrmAdministrador(); 
                    this.Hide();
                    oAdmin.Show();
                    break;

                case "Entrenador":
                    FrmEntrenador  oEntren = new FrmEntrenador();
                    this.Hide();
                    oEntren .Show();
                    break;
                case "Usuario":
                    FrmUsuario oUsuario = new FrmUsuario();
                    this.Hide();
                    oUsuario.Show();
                    break;

                default:
                    break;
            }


        }
    }
}
